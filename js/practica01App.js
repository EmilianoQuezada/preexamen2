async function cargarRazas() {
    try {
        const response = await fetch('https://dog.ceo/api/breeds/list');
        const data = await response.json();
        const breeds = data.message;
        const select = document.getElementById('cbRaza');
        select.innerHTML = ""; 
        breeds.forEach(breed => {
            const option = document.createElement('option');
            option.value = breed;
            option.textContent = breed;
            select.appendChild(option);
        });
    } catch (error) {
        console.error('Error al cargar el fetch: ', error);
    }
}

// Función para obtener y mostrar la imagen del perro seleccionado
async function cargarImagen() {
    const Raza = document.getElementById('cbRaza').value;
    const imagen = document.getElementById('imagen');

    if (Raza) {
        try {
            // Obtener la imagen del perro de la API
            const response = await fetch(`https://dog.ceo/api/breed/${Raza}/images/random`);
            const data = await response.json();
            const imagenUrl = data.message;

            // Mostrar la imagen del perro
            imagen.src = imagenUrl;
        } catch (error) {
            console.error('Error al obtener la imagen del perro:', error);
        }
    } else {
        // Si no se selecciona ninguna raza, mostrar un mensaje de error
        console.error('Por favor, selecciona una opcion.');
    }
}

// Asociar la función cargarRazas al evento click del botón "Cargar Razas"
document.getElementById('btnCargar').addEventListener('click', cargarRazas);

// Asociar la función verImagen al evento click del botón "Ver Imagen"
document.getElementById('btnImagen').addEventListener('click', cargarImagen);